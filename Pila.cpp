#include <iostream>
#include "Pila.h"
using namespace std;


Pila::Pila(){
  int numero = 0;
  bool band;
}

Pila::Pila(int numero, bool band){
  this -> numero = numero;
  this -> band = band;
}

// Funcion que verifica si la pila esta vacia o no.
void Pila::pila_vacia(int tope){
  if (tope == 0){ 
    // La pila esta vacia
    band = true;
  }
  else{
    // La pila no esta vacia
    band = false;
  }
}

// Funcion que verifica si la pila esta llena o no.
void Pila::pila_llena(int tope, int tamano){
  if (tope == tamano){
    // La pila esta llena
    band = true;
  }
  else{
    // La pila no esta llena
    band = false;
  }
}

// Funcion push que permitira el ingreso de elementos a la pila.
void Pila::push(int pila[], int &tope, int tamano, int &numero){
  pila_llena(tope, tamano);
  // Si la pila no esta llena se agregaran elementos.
  if(band == false){
    tope++;
    pila[tope]=numero;
  }
  else{
    cout << "Desbordamiento, pila llena" << endl;
  }
}

// Funcion pop que permitira la eliminacion de elementos en la pila.
void Pila::pop(int pila[], int &tope){
  pila_vacia(tope);
  // Si la pila esta vacia.
  if (band == true){
    cout << "Subdesbordamiento, Pila vacı́a"<< endl;
  }
  else{
    cout << "Elemento eliminado: " << pila[tope] << endl;
    tope--;
  }
}

// Funcion que imprime la pila.
void Pila::ver_pila(int pila[], int tope){
  if(tope > 0){
    for (int i=tope; i>0; i--){
      cout << "|" << pila[i] << "|" << endl;
    }
  }
  else{
    cout << "La pila esta vacia" << endl;
  }
}
