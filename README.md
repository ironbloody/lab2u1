**Laboratiorio 2 Unidad 1**                                                                                             
Programa basado en programación orientada a objetos y arreglos.                                                                           

**Pre-requisitos**                                                                                                                                    
Cplusplus                                                                                                                                                
Make                                                                                                                                             
Ubuntu                                                                                                                                                                                                                                                                            
**Instalación**                                                                                                                                       
Instalar Make si no esta en su computador.                                

Para instalar Make inicie la terminal y escriba lo siguiente:

sudo apt install make

**Ejecutando**                                                                                                                                         
Para abrir el programa necesita ejecutar el archivo make desde la terminal, luego escribir en la terminal lo siguiente: ./programa.
Una vez abierto el progroama le pedira ingresar el tamaño de la pila, una vez ingresado le mostrara un menu con 4 opciones diferentes, la opcion 1 le serviera para ingresar elementos a la pila, la opcion 2 le servirara para remover elementos de la pila, la opcion 3 le perimitira ver la pila y la ultima le dejara salir del programa.

**Construido con**                                                                                                                                    
C++                                                                                                                                      
                                                                                                                                         
Librerias:                                                                                                                               
Iostream                                                                                                                              

**Versionado**                                                                                                                                        
Version 1.3                                                                                                                                        

**Autores**                                                                                                                                                                                                                                                 
Rodrigo Valenzuela                                                                                                                                                                                               


