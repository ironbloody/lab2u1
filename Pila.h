#include <iostream>
using namespace std;

#ifndef PILA_H
#define PILA_H


class Pila {
  
  // Metodos privados
  private:
    int numero = 0;
    bool band;
    
  // Metodos publicos
  public:
    Pila();
    Pila(int numero, bool band);
    
    // Funciones
    void pila_vacia(int tope);
    void pila_llena(int tope, int tamano);
    void push(int pila[], int &tope, int tamano, int &numero);
    void pop(int pila[], int &tope);
    void ver_pila(int pila[], int tope);
  
  
  
  
};
#endif