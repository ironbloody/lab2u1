#include <iostream>
#include "Pila.h"
using namespace std;

// Funcion menu que llamara a las otras funciones.
void menu(int pila[], int tope, int tamano){
  int opcion;
  int numero;
  Pila p = Pila();
  
  do {
    cout << "---------------------------" << endl;
    cout << "1. Agregar/push" << endl;
    cout << "2. Remover/pop" << endl;
    cout << "3. Ver pila" << endl;
    cout << "4. salir" << endl;
    cout << "---------------------------" << endl;
    
    cout << "\nIngrese una opcion: ";
    cin >> opcion;
    
    switch (opcion) {
    case 1:
      cout << "Ingrese el numero a agregar: " << endl;
      cin >> numero;
      // Se llama a push.
      p.push(pila,tope,tamano,numero);
      break;
      
    case 2:
      // Se llama a pop.
      p.pop(pila, tope);
      break;
      
    case 3:
      // Se llama a ver_pila.
      p.ver_pila(pila, tope);
      break;
      
    }        
  } while (opcion != 4);
}

// Main
int main() {
  int tamano;
  //Se ingresa el tamaño de la pila
  cout << "Ingrese el tamaño: " << endl;
  cin >> tamano;
  // Si el tamaño es mayor a 0 el tope sera 0
  if (tamano > 0){
    int tope = 0;
    // Se crea la pila con el tamaño correspondiente.
    int pila[tamano];
    // Se llama al menu.
    menu(pila, tope, tamano);
  }
  return 0;
}




